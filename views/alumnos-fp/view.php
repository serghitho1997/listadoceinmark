<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AlumnosFp */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Alumnos Fp', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="alumnos-fp-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Quiere borrar este registro?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'fecha',
            'nombre_completo',
            'email:email',
            'telefono',
            'curso',
            'nivel_formativo',
            'origen',
            'introduce_datos',
            'comentarios',
            'estado',
             'antiguo_alumno',
            'darBaja',
        ],
    ]) ?>

</div>
