<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AlumnosFp */

$this->title = 'Añadir nuevo Alumno Fp';
$this->params['breadcrumbs'][] = ['label' => 'Alumnos Fp', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumnos-fp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
