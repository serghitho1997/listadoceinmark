<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ocupados */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ocupados-form">

    <?php $form = ActiveForm::begin(); ?>

   

    <?= $form->field($model, 'fecha')->textInput() ?>

    <?= $form->field($model, 'nombre_completo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput() ?>

    <?= $form->field($model, 'nombre_curso')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nivel_formativo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'antiguo_alumno')->textInput() ?>

    <?= $form->field($model, 'darBaja')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
