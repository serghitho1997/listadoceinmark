<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Desempleados */

$this->title = 'Añadir alumno curso Desempleados';
$this->params['breadcrumbs'][] = ['label' => 'Desempleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desempleados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
