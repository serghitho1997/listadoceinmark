<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Desempleados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desempleados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir alumno', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'fecha',
            'nombre_completo',
            'email:email',
            'telefono',
            'curso',
            'nivel_formativo',
            'origen',
            'comentarios',
            'antiguo_alumno' => [
            
            'label'=>'antiguo alumno',

            'format'=>'raw',

             'value' => function($model, $key, $index, $column) { return $model->antiguo_alumno == false ? 'No' : 'Sí';}],
             'darBaja' => [
             
              'label'=>'dado de baja',

            'format'=>'raw',

             'value' => function($model, $key, $index, $column) { return $model->darBaja == false ? 'No' : 'Sí';}],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
