<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Desempleados */

$this->title = 'Actualizar alumnos curso Desempleados: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Desempleados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="desempleados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
