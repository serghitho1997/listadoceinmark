<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "desempleados".
 *
 * @property int $id
 * @property string|null $fecha
 * @property string|null $nombre_completo
 * @property string|null $email
 * @property int|null $telefono
 * @property string|null $curso
 * @property string|null $nivel_formativo
 * @property string|null $origen
 * @property string|null $comentarios
 * @property int|null $antiguo_alumno
 * @property int|null $darBaja
 */
class Desempleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'desempleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['id', 'telefono', 'antiguo_alumno', 'darBaja'], 'integer'],
            [['fecha'], 'required'],
            [['fecha'], 'safe'],
            [['nombre_completo'], 'required'],
            [['nombre_completo', 'email', 'curso', 'nivel_formativo', 'origen'], 'string', 'max' => 500],
            [['comentarios'], 'string', 'max' => 2000],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha de inscripción',
            'nombre_completo' => 'Nombre Completo',
            'email' => 'Email',
            'telefono' => 'Teléfono',
            'curso' => 'Curso',
            'nivel_formativo' => 'Nivel formativo',
            'origen' => 'Origen',
            'comentarios' => 'Comentarios',
            'antiguo_alumno' => 'Antiguo alumno',
            'darBaja' => 'Dar de baja',
        ];
    }
}
