<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ocupados".
 *
 * @property int $id
 * @property string|null $fecha
 * @property string|null $nombre_completo
 * @property string|null $email
 * @property int|null $telefono
 * @property string|null $nombre_curso
 * @property string|null $nivel_formativo
 * @property int|null $antiguo_alumno
 * @property int|null $darBaja
 */
class Ocupados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ocupados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            
            [['telefono', 'antiguo_alumno', 'darBaja'], 'integer'],
            [['fecha'], 'safe'],
            [['fecha'], 'required'],
            [['nombre_completo'], 'required'],
            [['nombre_completo', 'email', 'nombre_curso', 'nivel_formativo'], 'string', 'max' => 500],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' =>'ID',
            'fecha' => 'Fecha de inscripción',
            'nombre_completo' => 'Nombre Completo',
            'email' => 'Email',
            'telefono' => 'Teléfono',
            'nombre_curso' => 'Nombre del curso',
            'nivel_formativo' => 'Nivel formativo',
            'antiguo_alumno' => 'Antiguo alumno',
            'darBaja' => 'Dar de baja',
        ];
    }
}
