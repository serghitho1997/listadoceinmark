<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos_fp".
 *
 * @property int $id
 * @property string|null $fecha
 * @property string|null $nombre_completo
 * @property string|null $email
 * @property int|null $telefono
 * @property string|null $curso
 * @property string|null $nivel_formativo
 * @property string|null $origen
 * @property string|null $introduce_datos
 * @property string|null $comentarios
 * @property string|null $estado
 * @property int|null $antiguo_alumno
 * @property int|null $darBaja
 */
class AlumnosFp extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos_fp';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           
            [['id', 'telefono', 'antiguo_alumno', 'darBaja'], 'integer'],
            [['fecha'], 'safe'],
            [['fecha'], 'required'],
            [['nombre_completo'], 'required'],
            [['nombre_completo', 'email', 'curso', 'nivel_formativo', 'origen', 'introduce_datos', 'estado'], 'string', 'max' => 500],
            [['comentarios'], 'string', 'max' => 2000],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha de inscripción',
            'nombre_completo' => 'Nombre Completo',
            'email' => 'Email',
            'telefono' => 'Teléfono',
            'curso' => 'Curso',
            'nivel_formativo' => 'Nivel Formativo',
            'origen' => 'Origen',
            'introduce_datos' => 'Introduce Datos',
            'comentarios' => 'Comentarios',
            'estado' => 'Estado',
            'antiguo_alumno' => 'Antiguo Alumno',
            'darBaja' => 'Dado de Baja',
        ];
    }
}
