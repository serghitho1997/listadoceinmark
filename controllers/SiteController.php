<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\AlumnosFp;
use app\models\Desempleados;
use app\models\Ocupados;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
        
      


    
    public function actionConsulta1(){
        
        $numero = Yii::$app->db
                ->createCommand("select count(fecha),count(nombre_completo),count(email),count(telefono)teléfono,count(curso),count(nivel_formativo),count(origen),count(introduce_datos),count(comentarios),count(estado),count(antiguo_alumno),count(darBaja) from alumnos_fp")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select fecha,nombre_completo,email,telefono as teléfono,curso,nivel_formativo,origen,introduce_datos,comentarios,estado,antiguo_alumno,darBaja from alumnos_fp",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 500,
            ]
           
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['fecha','nombre_completo','email','teléfono','curso','nivel_formativo','origen','introduce_datos','comentarios','estado','antiguo_alumno','darBaja'],
            "titulo"=>"Alumnos FP",
            "enunciado"=>"lista alumnos 2020-2021",
           
           
        ]);
    }
    
    public function actionConsulta2(){
        
        $numero = Yii::$app->db
                ->createCommand("select count(fecha),count(nombre_completo),count(email),count(telefono)teléfono,count(curso),count(nivel_formativo),count(origen),count(comentarios),count(antiguo_alumno),count(darBaja) from desempleados")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select fecha,nombre_completo,email,telefono as teléfono,curso,nivel_formativo,origen,comentarios,antiguo_alumno,darBaja from desempleados",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 500,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['fecha','nombre_completo','email','teléfono','curso','nivel_formativo','origen','comentarios','antiguo_alumno','darBaja'],
            "titulo"=>"Alumnos curso desempleados",
            "enunciado"=>"lista alumnos 2020-2021",
           
        ]);
    }
    
    public function actionConsulta3(){
        
        $numero = Yii::$app->db
                ->createCommand("select count(fecha),count(nombre_completo),count(email),count(telefono)teléfono,count(nombre_curso),count(antiguo_alumno),count(darBaja) from ocupados")
                ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>"select fecha,nombre_completo,email,telefono as teléfono,nombre_curso,nivel_formativo,antiguo_alumno,darBaja from ocupados",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 500,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['fecha','nombre_completo','email','teléfono','nombre_curso','nivel_formativo','antiguo_alumno','darBaja'],
            "titulo"=>"Alumnos curso ocupados",
            "enunciado"=>"lista alumnos 2020-2021",
           
        ]);
    }
}
